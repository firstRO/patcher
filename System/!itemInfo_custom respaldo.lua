tbl_items_custom = {
    -- kRO rings
	[40000] = {
		unidentifiedDisplayName = "Accessory",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = {
			"Unknown Item, can be identified by ^6A5ACDMagnifier^000000."
		},
		identifiedDisplayName = "kRO Swordman Necklace",
		identifiedResourceName = "��Ŭ����",
		identifiedDescriptionName = {
			"A necklace once worn by an ancient queen that changes the wearer's appearance.",
			"Class: ^777777Costume Accessory^000000",
			"Weight: ^7777770^000000",
			"Required Level: ^7777771^000000",
			"Jobs: ^777777Lord Knight and Paladin^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	[40001] = {
		unidentifiedDisplayName = "Accessory",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = {
			"Unknown Item, can be identified by ^6A5ACDMagnifier^000000."
		},
		identifiedDisplayName = "kRO Acolyte Rosary",
		identifiedResourceName = "���ڸ�",
		identifiedDescriptionName = {
			"A string of beads used to count the number of repetitions that changes the wearer's appearance.",
			"Class: ^777777Costume Accessory^000000",
			"Weight: ^7777770^000000",
			"Required Level: ^7777771^000000",
			"Jobs: ^777777High Priest and Champion^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	[40002] = {
		unidentifiedDisplayName = "Accessory",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = {
			"Unknown Item, can be identified by ^6A5ACDMagnifier^000000."
		},
		identifiedDisplayName = "kRO Mage Earring",
		identifiedResourceName = "�̾",
		identifiedDescriptionName = {
			"Jewelry, worn on the earlobe, that changes the wearer's appearance.",
			"Class: ^777777Costume Accessory^000000",
			"Defense: ^7777770^000000",
			"Weight: ^7777770^000000",
			"Required Level: ^7777771^000000",
			"Jobs: ^777777High Wizard and Professor^000000"
		},
		slotCount = 0,
		ClassNum = 0
    },
    [40003] = {
		unidentifiedDisplayName = "Accessory",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = {
			"Unknown Item, can be identified by ^6A5ACDMagnifier^000000."
		},
		identifiedDisplayName = "kRO Merchant Ring",
		identifiedResourceName = "��",
		identifiedDescriptionName = {
			"An ancient ring than changes the wearer's appearance.",
			"Class: ^777777Costume Accessory^000000",
			"Defense: ^7777770^000000",
			"Weight: ^7777770^000000",
			"Required Level: ^7777771^000000",
			"Jobs: ^777777High Wizard and Professor^000000"
		},
		slotCount = 0,
		ClassNum = 0
    },
    [40004] = {
		unidentifiedDisplayName = "Accessory",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = {
			"Unknown Item, can be identified by ^6A5ACDMagnifier^000000."
		},
		identifiedDisplayName = "kRO Archer Glove",
		identifiedResourceName = "�۷���",
		identifiedDescriptionName = {
			"An ancient mitten than changes the wearer's appearance.",
			"Class: ^777777Costume Accessory^000000",
			"Defense: ^7777770^000000",
			"Weight: ^7777770^000000",
			"Required Level: ^7777771^000000",
			"Jobs: ^777777High Wizard and Professor^000000"
		},
		slotCount = 0,
		ClassNum = 0
    },
    [40005] = {
		unidentifiedDisplayName = "Accessory",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = {
			"Unknown Item, can be identified by ^6A5ACDMagnifier^000000."
		},
		identifiedDisplayName = "kRO Thief Brooch",
		identifiedResourceName = "���ġ",
		identifiedDescriptionName = {
			"An ancient brooch than changes the wearer's appearance.",
			"Class: ^777777Costume Accessory^000000",
			"Defense: ^7777770^000000",
			"Weight: ^7777770^000000",
			"Required Level: ^7777771^000000",
			"Jobs: ^777777High Wizard and Professor^000000"
		},
		slotCount = 0,
		ClassNum = 0
    },
    -- jRO rings
	[40006] = {
		unidentifiedDisplayName = "Accessory",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = {
			"Unknown Item, can be identified by ^6A5ACDMagnifier^000000."
		},
		identifiedDisplayName = "jRO Swordman Necklace",
		identifiedResourceName = "��Ŭ����",
		identifiedDescriptionName = {
			"A necklace once worn by an ancient queen that changes the wearer's appearance.",
			"Class: ^777777Costume Accessory^000000",
			"Weight: ^7777770^000000",
			"Required Level: ^7777771^000000",
			"Jobs: ^777777Lord Knight and Paladin^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	[40007] = {
		unidentifiedDisplayName = "Accessory",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = {
			"Unknown Item, can be identified by ^6A5ACDMagnifier^000000."
		},
		identifiedDisplayName = "jRO Acolyte Rosary",
		identifiedResourceName = "���ڸ�",
		identifiedDescriptionName = {
			"A string of beads used to count the number of repetitions that changes the wearer's appearance.",
			"Class: ^777777Costume Accessory^000000",
			"Weight: ^7777770^000000",
			"Required Level: ^7777771^000000",
			"Jobs: ^777777High Priest and Champion^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	[40008] = {
		unidentifiedDisplayName = "Accessory",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = {
			"Unknown Item, can be identified by ^6A5ACDMagnifier^000000."
		},
		identifiedDisplayName = "jRO Mage Earring",
		identifiedResourceName = "�̾",
		identifiedDescriptionName = {
			"Jewelry, worn on the earlobe, that changes the wearer's appearance.",
			"Class: ^777777Costume Accessory^000000",
			"Defense: ^7777770^000000",
			"Weight: ^7777770^000000",
			"Required Level: ^7777771^000000",
			"Jobs: ^777777High Wizard and Professor^000000"
		},
		slotCount = 0,
		ClassNum = 0
    },
    [40009] = {
		unidentifiedDisplayName = "Accessory",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = {
			"Unknown Item, can be identified by ^6A5ACDMagnifier^000000."
		},
		identifiedDisplayName = "jRO Merchant Ring",
		identifiedResourceName = "��",
		identifiedDescriptionName = {
			"An ancient ring than changes the wearer's appearance.",
			"Class: ^777777Costume Accessory^000000",
			"Defense: ^7777770^000000",
			"Weight: ^7777770^000000",
			"Required Level: ^7777771^000000",
			"Jobs: ^777777High Wizard and Professor^000000"
		},
		slotCount = 0,
		ClassNum = 0
    },
    [40010] = {
		unidentifiedDisplayName = "Accessory",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = {
			"Unknown Item, can be identified by ^6A5ACDMagnifier^000000."
		},
		identifiedDisplayName = "jRO Archer Glove",
		identifiedResourceName = "�۷���",
		identifiedDescriptionName = {
			"An ancient mitten than changes the wearer's appearance.",
			"Class: ^777777Costume Accessory^000000",
			"Defense: ^7777770^000000",
			"Weight: ^7777770^000000",
			"Required Level: ^7777771^000000",
			"Jobs: ^777777High Wizard and Professor^000000"
		},
		slotCount = 0,
		ClassNum = 0
    },
    [40011] = {
		unidentifiedDisplayName = "Accessory",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = {
			"Unknown Item, can be identified by ^6A5ACDMagnifier^000000."
		},
		identifiedDisplayName = "jRO Thief Brooch",
		identifiedResourceName = "���ġ",
		identifiedDescriptionName = {
			"An ancient brooch than changes the wearer's appearance.",
			"Class: ^777777Costume Accessory^000000",
			"Defense: ^7777770^000000",
			"Weight: ^7777770^000000",
			"Required Level: ^7777771^000000",
			"Jobs: ^777777High Wizard and Professor^000000"
		},
		slotCount = 0,
		ClassNum = 0
    },
    -- Classic rings
	[40012] = {
		unidentifiedDisplayName = "Accessory",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = {
			"Unknown Item, can be identified by ^6A5ACDMagnifier^000000."
		},
		identifiedDisplayName = "Classic Swordman Necklace",
		identifiedResourceName = "��Ŭ����",
		identifiedDescriptionName = {
			"A necklace once worn by an ancient queen that changes the wearer's appearance.",
			"Class: ^777777Costume Accessory^000000",
			"Weight: ^7777770^000000",
			"Required Level: ^7777771^000000",
			"Jobs: ^777777Lord Knight and Paladin^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	[40013] = {
		unidentifiedDisplayName = "Accessory",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = {
			"Unknown Item, can be identified by ^6A5ACDMagnifier^000000."
		},
		identifiedDisplayName = "Classic Acolyte Rosary",
		identifiedResourceName = "���ڸ�",
		identifiedDescriptionName = {
			"A string of beads used to count the number of repetitions that changes the wearer's appearance.",
			"Class: ^777777Costume Accessory^000000",
			"Weight: ^7777770^000000",
			"Required Level: ^7777771^000000",
			"Jobs: ^777777High Priest and Champion^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	[40014] = {
		unidentifiedDisplayName = "Accessory",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = {
			"Unknown Item, can be identified by ^6A5ACDMagnifier^000000."
		},
		identifiedDisplayName = "Classic Mage Earring",
		identifiedResourceName = "�̾",
		identifiedDescriptionName = {
			"Jewelry, worn on the earlobe, that changes the wearer's appearance.",
			"Class: ^777777Costume Accessory^000000",
			"Defense: ^7777770^000000",
			"Weight: ^7777770^000000",
			"Required Level: ^7777771^000000",
			"Jobs: ^777777High Wizard and Professor^000000"
		},
		slotCount = 0,
		ClassNum = 0
    },
    [40015] = {
		unidentifiedDisplayName = "Accessory",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = {
			"Unknown Item, can be identified by ^6A5ACDMagnifier^000000."
		},
		identifiedDisplayName = "Classic Merchant Ring",
		identifiedResourceName = "��",
		identifiedDescriptionName = {
			"An ancient ring than changes the wearer's appearance.",
			"Class: ^777777Costume Accessory^000000",
			"Defense: ^7777770^000000",
			"Weight: ^7777770^000000",
			"Required Level: ^7777771^000000",
			"Jobs: ^777777High Wizard and Professor^000000"
		},
		slotCount = 0,
		ClassNum = 0
    },
    [40016] = {
		unidentifiedDisplayName = "Accessory",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = {
			"Unknown Item, can be identified by ^6A5ACDMagnifier^000000."
		},
		identifiedDisplayName = "Classic Archer Glove",
		identifiedResourceName = "�۷���",
		identifiedDescriptionName = {
			"An ancient mitten than changes the wearer's appearance.",
			"Class: ^777777Costume Accessory^000000",
			"Defense: ^7777770^000000",
			"Weight: ^7777770^000000",
			"Required Level: ^7777771^000000",
			"Jobs: ^777777High Wizard and Professor^000000"
		},
		slotCount = 0,
		ClassNum = 0
    },
    [40017] = {
		unidentifiedDisplayName = "Accessory",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = {
			"Unknown Item, can be identified by ^6A5ACDMagnifier^000000."
		},
		identifiedDisplayName = "Classic Thief Brooch",
		identifiedResourceName = "���ġ",
		identifiedDescriptionName = {
			"An ancient brooch than changes the wearer's appearance.",
			"Class: ^777777Costume Accessory^000000",
			"Defense: ^7777770^000000",
			"Weight: ^7777770^000000",
			"Required Level: ^7777771^000000",
			"Jobs: ^777777High Wizard and Professor^000000"
		},
		slotCount = 0,
		ClassNum = 0
    },
	
	-- 1st quest and others
	[40050] = {
		unidentifiedDisplayName = "Salamander Card",
		unidentifiedResourceName = "�̸�����ī��",
		unidentifiedDescriptionName = { "..." },
		identifiedDisplayName = "1stRO Salamander Card",
		identifiedResourceName = "�̸�����ī��",
		identifiedDescriptionName = {
			"A custom item created by 1stRO",
			"Increases damage of the ^008800Fire Pillar^000000 and ^008800Meteor Storm^000000 skills by 40%.",
			"Class:^6666CC Card^000000",
			"Compound on:^008C99 Garment^000000",
			"Weight:^009900 1^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	[40051] = {
		unidentifiedDisplayName = "Gemini-S58 Card",
		unidentifiedResourceName = "�̸�����ī��",
		unidentifiedDescriptionName = { "..." },
		identifiedDisplayName = "1stRO Gemini-S58 Card",
		identifiedResourceName = "�̸�����ī��",
		identifiedDescriptionName = {
			"A custom item created by 1stRO",
			"Adds 30% resistance to the ^FF0000Stun^000000 and ^FF0000Silence^000000 if base AGI is 90 or higher.",
			"adds 50% resistance to the ^663399Stone Curse^000000 and ^FF0000Sleep^000000 if base VIT is 80 or higher.",
			"Class:^6666CC Card^000000",
			"Compound on:^6600FF Headgear^000000",
			"Weight:^009900 1^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	[40052] = {
		unidentifiedDisplayName = "Gold Coin",
		unidentifiedResourceName = "��ȭ",
		unidentifiedDescriptionName = { "..." },
		identifiedDisplayName = "Gold Cash Coin",
		identifiedResourceName = "��ȭ",
		identifiedDescriptionName = { "1st RO: 10.000" },
		slotCount = 0,
		ClassNum = 0
	},
	[40053] = {
		unidentifiedDisplayName = "Silver Coin",
		unidentifiedResourceName = "��ȭ",
		unidentifiedDescriptionName = { "..." },
		identifiedDisplayName = "Silver Cash Coin",
		identifiedResourceName = "��ȭ",
		identifiedDescriptionName = { "1st RO: 5.000" },
		slotCount = 0,
		ClassNum = 0
	},
	[40054] = {
		unidentifiedDisplayName = "Bronze Chocolate",
		unidentifiedResourceName = "��ȭ",
		unidentifiedDescriptionName = { "..." },
		identifiedDisplayName = "Bronze Cash Coin",
		identifiedResourceName = "��ȭ",
		identifiedDescriptionName = { "1st RO: 1.000" },
		slotCount = 0,
		ClassNum = 0
	},
	[40055] = {
		unidentifiedDisplayName = "Mvp Ticket",
		unidentifiedResourceName = "C_Mvp",
		unidentifiedDescriptionName = { "..." },
		identifiedDisplayName = "Mvp Ticket",
		identifiedResourceName = "C_Mvp",
		identifiedDescriptionName = { "1st RO: Mvp ticket!" },
		slotCount = 0,
		ClassNum = 0
	},


	-- Piercing System
	[40100] = {
		unidentifiedDisplayName = "Mask",
		unidentifiedResourceName = "스마일",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Alarm Mask",
		identifiedResourceName = "알람가면",
		identifiedDescriptionName = {
			"A mask which resembles the face of an Alarm monster.",
			"Increases resistance against ^663399Blind^000000 status by 25%.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 2^000000",
			"Position:^6666CC Middle, Lower^000000",
			"Weight:^009900 10^000000",
			"Level Requirement:^009900 1^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 1,
		ClassNum = 170
	},
	[40101] = {
		unidentifiedDisplayName = "Glasses",
		unidentifiedResourceName = "글래스",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Angel Spirit",
		identifiedResourceName = "천사의유령",
		identifiedDescriptionName = {
			"A golden mask rumored to be worn by some ancient Goddess.",
			"Hit +8, STR +1",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Middle^000000",
			"Weight:^009900 20^000000",
			"Level Requirement:^009900 30^000000",
			"Jobs:^6666CC All^000000"
		},
		slotCount = 1,
		ClassNum = 394
	},
	[40102] = {
		unidentifiedDisplayName = "Glasses",
		unidentifiedResourceName = "글래스",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Angel Wing Ears",
		identifiedResourceName = "천사날개귀",
		identifiedDescriptionName = {
			"An ear accesory that looks just like white, beautiful angel wings.",
			"Str +1",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 1^000000",
			"Position:^6666CC Middle^000000",
			"Weight:^009900 10^000000",
			"Level Requirement:^009900 70^000000",
			"Jobs:^6666CC All^000000"
		},
		slotCount = 1,
		ClassNum = 158
	},
	[40103] = {
		unidentifiedDisplayName = "Glasses",
		unidentifiedResourceName = "글래스",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Black Glasses",
		identifiedResourceName = "검은뿔테안경",
		identifiedDescriptionName = {
			"A black bold glasses. This is it- item for this season!",
			"INT + 1, MDEF +2.",
			"Class:^6666CC Costume^000000",
			"Location:^6666CC Middle^000000",
			"Weight:^009900 0^000000",
			"Level Requirement:^009900 1^000000",
			"Jobs:^6666CC All^000000"
		},
		slotCount = 1,
		ClassNum = 404
	},
	[40104] = {
		unidentifiedDisplayName = "Glasses",
		unidentifiedResourceName = "글래스",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Blinkers",
		identifiedResourceName = "눈가리개",
		identifiedDescriptionName = {
			"A face mask that is worn by those who have trouble sleeping.",
			"Increases resistance against Blind status by 50%.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Middle^000000",
			"Weight:^009900 10^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 1,
		ClassNum = 82
	},
	[40105] = {
		unidentifiedDisplayName = "Glasses",
		unidentifiedResourceName = "글래스",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Blush",
		identifiedResourceName = "발그레",
		identifiedDescriptionName = {
			"Two red dots that can be worn on the face to give a blushing impression.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Middle^000000",
			"Weight:^009900 10^000000",
			"Jobs:^6666CC All^000000"
		},
		slotCount = 1,
		ClassNum = 125
	},
	[40106] = {
		unidentifiedDisplayName = "Glasses",
		unidentifiedResourceName = "글래스",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Dark Blinkers",
		identifiedResourceName = "검은눈가리개",
		identifiedDescriptionName = {
			"A blinder enchanted with dark magic.",
			"Prevents ^663399Blind^000000 status.",
			"Increases resistance against ^663399Stun^000000 status by 2%.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Middle^000000",
			"Weight:^009900 10^000000",
			"Jobs:^6666CC Every Job except Novice^000000"
		},
		slotCount = 1,
		ClassNum = 187
	},
	[40107] = {
		unidentifiedDisplayName = "Glasses",
		unidentifiedResourceName = "글래스",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Elven Ears",
		identifiedResourceName = "요정의귀",
		identifiedDescriptionName = {
			"A fashion accessory that gives ears that vaunted, elvish look.",
			"For some, it is an incredible turn-on.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Middle^000000",
			"Weight:^009900 10^000000",
			"Level Requirement:^009900 70^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 1,
		ClassNum = 73
	},
	[40108] = {
		unidentifiedDisplayName = "Glasses",
		unidentifiedResourceName = "글래스",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Evil Wing Ears",
		identifiedResourceName = "악마날개귀",
		identifiedDescriptionName = {
			"As an ear accessory is made of black wings, feels proud of grace and charisma from devil. Wearer feels like get mystery power through the wing.",
			"Str +1",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 2^000000",
			"Position:^6666CC Middle^000000",
			"Weight:^009900 0^000000",
			"Level Requirement:^009900 1^000000",
			"Jobs:^6666CC All^000000"
		},
		slotCount = 1,
		ClassNum = 152
	},
	[40109] = {
		unidentifiedDisplayName = "Glasses",
		unidentifiedResourceName = "글래스",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Geek Glasses",
		identifiedResourceName = "스핀글래스",
		identifiedDescriptionName = {
			"A pair of glasses that make the wearer's eyes look like they're spinning without focus.",
			"Adds 8% resistance to the ^663399Blind^000000 status.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 1^000000",
			"Position:^6666CC Middle^000000",
			"Weight:^009900 10^000000",
			"Jobs:^6666CC All^000000"
		},
		slotCount = 1,
		ClassNum = 27
	},
	[40110] = {
		unidentifiedDisplayName = "Glasses",
		unidentifiedResourceName = "글래스",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Glasses",
		identifiedResourceName = "글래스",
		identifiedDescriptionName = {
			"Prescription glasses worn to compensate for defects in vision.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Middle^000000",
			"Weight:^009900 10^000000",
			"Jobs:^6666CC All^000000"
		},
		slotCount = 1,
		ClassNum = 3
	},
	[40111] = {
		unidentifiedDisplayName = "Glasses",
		unidentifiedResourceName = "글래스",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Ears of Ifrit",
		identifiedResourceName = "이프리트의귀",
		identifiedDescriptionName = {
			"A headgear which imitates Ifrit's ears, the spiritual owner of fire.",
			"Str +1, Int +1, Mdef +3",
			"Increase damage of Fire Bolt, Fire Pillar, and Meteor Storm skill by 2%.",
			"Increase damage of Bash, Pierce, and Magnum Break skill by 2%.",
			"Increases resistence against ^FF0000Fire^000000 elemental attacks by 5%.",
			"Decreases resistence against ^0000BBWater^000000 elemental attacks by 5%.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Middle^000000",
			"Weight:^009900 0^000000",
			"Level Requirement:^009900 70^000000",
			"Jobs:^6666CC Every Job except Novice^000000"
		},
		slotCount = 1,
		ClassNum = 422
	},
	[40112] = {
		unidentifiedDisplayName = "Glasses",
		unidentifiedResourceName = "글래스",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Ears of Ifrit",
		identifiedResourceName = "이프리트의귀",
		identifiedDescriptionName = {
			"A headgear which imitates Ifrit's ears, the spiritual owner of fire.",
			"Str +1, Int +1, Mdef +3",
			"Increase damage of Fire Bolt, Fire Pillar, and Meteor Storm skill by 2%.",
			"Increase damage of Bash, Pierce, and Magnum Break skill by 2%.",
			"Increases resistence against ^FF0000Fire^000000 elemental attacks by 5%.",
			"Decreases resistence against ^0000BBWater^000000 elemental attacks by 5%.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Middle^000000",
			"Weight:^009900 0^000000",
			"Level Requirement:^009900 70^000000",
			"Jobs:^6666CC Every Job except Novice^000000"
		},
		slotCount = 1,
		ClassNum = 422
	},
	[40113] = {
		unidentifiedDisplayName = "Glasses",
		unidentifiedResourceName = "글래스",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Machoman's Glasses",
		identifiedResourceName = "하트파운데이션",
		identifiedDescriptionName = {
			"70's tough guy style sunglasses that give its wearer a manly swagger.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 1^000000",
			"Position:^6666CC Middle^000000",
			"Weight:^009900 10^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 1,
		ClassNum = 92
	},
	[40114] = {
		unidentifiedDisplayName = "Glasses",
		unidentifiedResourceName = "글래스",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Butterfly Mask",
		identifiedResourceName = "나비가면",
		identifiedDescriptionName = {
			"A black leather mask, shaped like butterfly wings, that increases damage inflicted upon ^6666CCDemihuman^000000 monsters by 3%.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Middle^000000",
			"Weight:^009900 10^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 1,
		ClassNum = 78
	},
	[40115] = {
		unidentifiedDisplayName = "Glasses",
		unidentifiedResourceName = "글래스",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Masquerade",
		identifiedResourceName = "나비가면",
		identifiedDescriptionName = {
			"A black leather mask, shaped like butterfly wings, that increases damage inflicted upon ^6666CCDemihuman^000000 monsters by 3%.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Middle^000000",
			"Weight:^009900 10^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 1,
		ClassNum = 78
	},
	[40116] = {
		unidentifiedDisplayName = "Glasses",
		unidentifiedResourceName = "글래스",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Mini Glasses",
		identifiedResourceName = "미니글래스",
		identifiedDescriptionName = {
			"Glasses worn by true sophisticates.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 1^000000",
			"Position:^6666CC Middle^000000",
			"Weight:^009900 10^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 1,
		ClassNum = 47
	},
	[40117] = {
		unidentifiedDisplayName = "Mask",
		unidentifiedResourceName = "스마일",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Mr. Smile",
		identifiedResourceName = "스마일",
		identifiedDescriptionName = {
			"A simple mask with a smile carved into it.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 1^000000",
			"Position:^6666CC Mid,Lower^000000",
			"Weight:^009900 10^000000",
			"Jobs:^6666CC All^000000"
		},
		slotCount = 1,
		ClassNum = 65
	},
	[40118] = {
		unidentifiedDisplayName = "Mask",
		unidentifiedResourceName = "스마일",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Opera Mask",
		identifiedResourceName = "오페라가면",
		identifiedDescriptionName = {
			"A mysterious mask traditionally worn by opera phantoms.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 2^000000",
			"Position:^6666CC Middle, Lower^000000",
			"Weight:^009900 20^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 1,
		ClassNum = 68
	},
	[40119] = {
		unidentifiedDisplayName = "Mask",
		unidentifiedResourceName = "스마일",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Scuba Mask",
		identifiedResourceName = "스쿠버마스크",
		identifiedDescriptionName = {
			"Scuba Glasses meant for Divers.",
			"If you wear this on the Ground, people will think you are a person not taking outside views seriously.",
			"Resistance against Water Element by 10%.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Middle^000000",
			"Weight:^009900 10^000000",
			"Level Requirement:^009900 1^000000",
			"Jobs:^6666CC All^000000"
		},
		slotCount = 1,
		ClassNum = 400
	},
	[40120] = {
		unidentifiedDisplayName = "Ribbon",
		unidentifiedResourceName = "리본",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Small Ribbons",
		identifiedResourceName = "양쪽귀밑리본",
		identifiedDescriptionName = {
			"A pair of small red ribbons which can be used to decorate the hair of little girls.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 1^000000",
			"Position:^6666CC Middle^000000",
			"Weight:^009900 10^000000",
			"Level Requirement:^009900 45^000000",
			"Jobs:^6666CC All^000000"
		},
		slotCount = 1,
		ClassNum = 169
	},
	-- 1st Costumes
	
	[40200] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Black Angel Wing Ears",
		identifiedResourceName = "Black_Angel_Wing_Ears",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5000
	},

	[40201] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Black Cowboy Hat",
		identifiedResourceName = "Black_Cowboy_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5001
	},

	[40202] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Black Feather Beret",
		identifiedResourceName = "Black_Feather_Beret",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5002
	},

	[40203] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Black Galapago Cap",
		identifiedResourceName = "Black_Galapago_Cap",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5003
	},

	[40204] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Black Gangster Scarf",
		identifiedResourceName = "Black_Gangster_Scarf",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5004
	},

	[40205] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Black Love Dad Bandana",
		identifiedResourceName = "Black_Love_Dad_Bandana",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5005
	},

	[40206] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Black Luxury Hat",
		identifiedResourceName = "Black_Luxury_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5006
	},

	[40207] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Black Neko Mimi",
		identifiedResourceName = "Black_Neko_Mimi",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5007
	},

	[40208] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Black Pirate Hat",
		identifiedResourceName = "Black_Pirate_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5008
	},

	[40209] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Black Scarf",
		identifiedResourceName = "Black_Scarf",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5009
	},

	[40210] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Black Ship Captain Hat",
		identifiedResourceName = "Black_Ship_Captain_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5010
	},

	[40211] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Black Valkyrie Helm",
		identifiedResourceName = "Black_Valkyrie_Helm",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5011
	},

	[40212] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Blue Angel Wing Ears",
		identifiedResourceName = "Blue_Angel_Wing_Ears",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5012
	},

	[40213] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Blue Cowboy Hat",
		identifiedResourceName = "Blue_Cowboy_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5013
	},

	[40214] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Blue Galapago Cap",
		identifiedResourceName = "Blue_Galapago_Cap",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5014
	},

	[40215] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Blue Gangster Scarf",
		identifiedResourceName = "Blue_Gangster_Scarf",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5015
	},

	[40216] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Blue Love Dad Bandana",
		identifiedResourceName = "Blue_Love_Dad_Bandana",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5016
	},

	[40217] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Blue Luxury Hat",
		identifiedResourceName = "Blue_Luxury_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5017
	},

	[40218] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Blue Neko Mimi",
		identifiedResourceName = "Blue_Neko_Mimi",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5018
	},

	[40219] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Blue Pirate Hat",
		identifiedResourceName = "Blue_Pirate_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5019
	},

	[40220] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Blue Scarf",
		identifiedResourceName = "Blue_Scarf",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5020
	},

	[40221] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Blue Ship Captain Hat",
		identifiedResourceName = "Blue_Ship_Captain_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5021
	},

	[40222] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Blue Valkyrie Helm",
		identifiedResourceName = "Blue_Valkyrie_Helm",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5022
	},

	[40223] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Brown Feather Beret",
		identifiedResourceName = "Brown_Feather_Beret",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5023
	},

	[40224] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Brown Galapago Cap",
		identifiedResourceName = "Brown_Galapago_Cap",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5024
	},

	[40225] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Brown Neko Mimi",
		identifiedResourceName = "Brown_Neko_Mimi",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5025
	},

	[40226] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Brown Valkyrie Helm",
		identifiedResourceName = "Brown_Valkyrie_Helm",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5026
	},

	[40227] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Cyan Angel Wing Ear",
		identifiedResourceName = "Cyan_Angel_Wing_Ear",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5027
	},

	[40228] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Cyan Angel Wing Ears",
		identifiedResourceName = "Cyan_Angel_Wing_Ears",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5028
	},

	[40229] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Cyan Cowboy Hat",
		identifiedResourceName = "Cyan_Cowboy_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5029
	},

	[40230] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Cyan Galapago Cap",
		identifiedResourceName = "Cyan_Galapago_Cap",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5030
	},

	[40231] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Cyan Gangster Scarf",
		identifiedResourceName = "Cyan_Gangster_Scarf",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5031
	},

	[40232] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Cyan Love Dad Bandana",
		identifiedResourceName = "Cyan_Love_Dad_Bandana",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5032
	},

	[40233] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Cyan Luxury Hat",
		identifiedResourceName = "Cyan_Luxury_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5033
	},

	[40234] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Cyan Neko Mimi",
		identifiedResourceName = "Cyan_Neko_Mimi",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5034
	},

	[40235] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Cyan Pirate Hat",
		identifiedResourceName = "Cyan_Pirate_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5035
	},

	[40236] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Cyan Scarf",
		identifiedResourceName = "Cyan_Scarf",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5036
	},

	[40237] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Cyan Ship Captain Hat",
		identifiedResourceName = "Cyan_Ship_Captain_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5037
	},

	[40238] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Cyan Valkyrie Helm",
		identifiedResourceName = "Cyan_Valkyrie_Helm",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5038
	},

	[40239] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Fox Ears",
		identifiedResourceName = "Fox_Ears",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5039
	},

	[40240] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Galapago Cap",
		identifiedResourceName = "Galapago_Cap",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5040
	},

	[40241] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "GBH Black",
		identifiedResourceName = "GBH_Black",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5041
	},

	[40242] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "GBH Blue",
		identifiedResourceName = "GBH_Blue",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5042
	},

	[40243] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "GBH Brown",
		identifiedResourceName = "GBH_Brown",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5043
	},

	[40244] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "GBH Cyan",
		identifiedResourceName = "GBH_Cyan",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5044
	},

	[40245] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "GBH Green",
		identifiedResourceName = "GBH_Green",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5045
	},

	[40246] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "GBH Purple",
		identifiedResourceName = "GBH_Purple",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5046
	},

	[40247] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "GBH Red",
		identifiedResourceName = "GBH_Red",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5047
	},

	[40248] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "GBH White",
		identifiedResourceName = "GBH_White",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5048
	},

	[40249] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "GBH Yellow",
		identifiedResourceName = "GBH_Yellow",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5049
	},

	[40250] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Gold Angel Wing Ears",
		identifiedResourceName = "Gold_Angel_Wing_Ears",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5050
	},

	[40251] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Gold Cowboy Hat",
		identifiedResourceName = "Gold_Cowboy_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5051
	},

	[40252] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Gold Feather Beret",
		identifiedResourceName = "Gold_Feather_Beret",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5052
	},

	[40253] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Gold Galapago Cap",
		identifiedResourceName = "Gold_Galapago_Cap",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5053
	},

	[40254] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Gold Gangster Scarf",
		identifiedResourceName = "Gold_Gangster_Scarf",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5054
	},

	[40255] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Gold Luxury Hat",
		identifiedResourceName = "Gold_Luxury_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5055
	},

	[40256] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Gold Neko Mimi",
		identifiedResourceName = "Gold_Neko_Mimi",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5056
	},

	[40257] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Gold Pirate Hat",
		identifiedResourceName = "Gold_Pirate_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5057
	},

	[40258] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Gold Scarf",
		identifiedResourceName = "Gold_Scarf",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5058
	},

	[40259] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Gold Ship Captain Hat",
		identifiedResourceName = "Gold_Ship_Captain_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5059
	},

	[40260] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Gold Valkyrie Helm",
		identifiedResourceName = "Gold_Valkyrie_Helm",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5060
	},

	[40261] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Green Angel Wing Ears",
		identifiedResourceName = "Green_Angel_Wing_Ears",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5061
	},

	[40262] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Green Cowboy Hat",
		identifiedResourceName = "Green_Cowboy_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5062
	},

	[40263] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Green Feather Beret",
		identifiedResourceName = "Green_Feather_Beret",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5063
	},

	[40264] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Green Galapago Cap",
		identifiedResourceName = "Green_Galapago_Cap",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5064
	},

	[40265] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Green Gangster Scarf",
		identifiedResourceName = "Green_Gangster_Scarf",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5065
	},

	[40266] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Green Love Dad Bandana",
		identifiedResourceName = "Green_Love_Dad_Bandana",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5066
	},

	[40267] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Green Luxury Hat",
		identifiedResourceName = "Green_Luxury_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5067
	},

	[40268] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Green Neko Mimi",
		identifiedResourceName = "Green_Neko_Mimi",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5068
	},

	[40269] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Green Pirate Hat",
		identifiedResourceName = "Green_Pirate_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5069
	},

	[40270] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Green Scarf",
		identifiedResourceName = "Green_Scarf",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5070
	},

	[40271] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Green Ship Captain Hat",
		identifiedResourceName = "Green_Ship_Captain_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5071
	},

	[40272] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Green Valkyrie Helm",
		identifiedResourceName = "Green_Valkyrie_Helm",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5072
	},

	[40273] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Halloween Scarf",
		identifiedResourceName = "Halloween_Scarf",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5073
	},

	[40274] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "LBH Black",
		identifiedResourceName = "LBH_Black",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5074
	},

	[40275] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "LBH Blue",
		identifiedResourceName = "LBH_Blue",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5075
	},

	[40276] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "LBH Blue Light",
		identifiedResourceName = "LBH_Blue_Light",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5076
	},

	[40277] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "LBH Gold",
		identifiedResourceName = "LBH_Gold",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5077
	},

	[40278] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "LBH Green",
		identifiedResourceName = "LBH_Green",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5078
	},

	[40279] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "LBH Green Light",
		identifiedResourceName = "LBH_Green_Light",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5079
	},

	[40280] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "LBH Pink",
		identifiedResourceName = "LBH_Pink",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5080
	},

	[40281] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "LBH Purple",
		identifiedResourceName = "LBH_Purple",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5081
	},

	[40282] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "LBH Red",
		identifiedResourceName = "LBH_Red",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5082
	},

	[40283] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "LBH Ruby",
		identifiedResourceName = "LBH_Ruby",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5083
	},

	[40284] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "LBH White",
		identifiedResourceName = "LBH_White",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5084
	},

	[40285] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "LBH Yellow",
		identifiedResourceName = "LBH_Yellow",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5085
	},

	[40286] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Light Blue Feather Beret",
		identifiedResourceName = "Light_Blue_Feather_Beret",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5086
	},

	[40287] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Love Dad Bandana",
		identifiedResourceName = "Love_Dad_Bandana",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5087
	},

	[40288] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Neko Mimi",
		identifiedResourceName = "Neko_Mimi",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5088
	},

	[40289] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Orange Feather Beret",
		identifiedResourceName = "Orange_Feather_Beret",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5089
	},

	[40290] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Orange Galapago Cap",
		identifiedResourceName = "Orange_Galapago_Cap",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5090
	},

	[40291] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Orange Neko Mimi",
		identifiedResourceName = "Orange_Neko_Mimi",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5091
	},

	[40292] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Orange Valkyrie Helm",
		identifiedResourceName = "Orange_Valkyrie_Helm",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5092
	},

	[40293] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Pink Feather Beret",
		identifiedResourceName = "Pink_Feather_Beret",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5093
	},

	[40294] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Pink Galapago Cap",
		identifiedResourceName = "Pink_Galapago_Cap",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5094
	},

	[40295] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Pink Neko Mimi",
		identifiedResourceName = "Pink_Neko_Mimi",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5095
	},

	[40296] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Pink Scarf",
		identifiedResourceName = "Pink_Scarf",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5096
	},

	[40297] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Pink Valkyrie Helm",
		identifiedResourceName = "Pink_Valkyrie_Helm",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5097
	},

	[40298] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Purple Angel Wing Ears",
		identifiedResourceName = "Purple_Angel_Wing_Ears",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5098
	},

	[40299] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Purple Cowboy Hat",
		identifiedResourceName = "Purple_Cowboy_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5099
	},

	[40300] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Purple Feather Beret",
		identifiedResourceName = "Purple_Feather_Beret",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5100
	},

	[40301] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Purple Galapago Cap",
		identifiedResourceName = "Purple_Galapago_Cap",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5101
	},

	[40302] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Purple Gangster Scarf",
		identifiedResourceName = "Purple_Gangster_Scarf",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5102
	},

	[40303] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Purple Love Dad Bandana",
		identifiedResourceName = "Purple_Love_Dad_Bandana",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5103
	},

	[40304] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Purple Luxury Hat",
		identifiedResourceName = "Purple_Luxury_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5104
	},

	[40305] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Purple Neko Mimi",
		identifiedResourceName = "Purple_Neko_Mimi",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5105
	},

	[40306] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Purple Pirate Hat",
		identifiedResourceName = "Purple_Pirate_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5106
	},

	[40307] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Purple Scarf",
		identifiedResourceName = "Purple_Scarf",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5107
	},

	[40308] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Purple Ship Captain Hat",
		identifiedResourceName = "Purple_Ship_Captain_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5108
	},

	[40309] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Purple Valkyrie Helm",
		identifiedResourceName = "Purple_Valkyrie_Helm",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5109
	},

	[40310] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Red Angel Wing Ears",
		identifiedResourceName = "Red_Angel_Wing_Ears",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5110
	},

	[40311] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Red Cowboy Hat",
		identifiedResourceName = "Red_Cowboy_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5111
	},

	[40312] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Red Feather Beret",
		identifiedResourceName = "Red_Feather_Beret",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5112
	},

	[40313] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Red Galapago Cap",
		identifiedResourceName = "Red_Galapago_Cap",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5113
	},

	[40314] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Red Love Dad Bandana",
		identifiedResourceName = "Red_Love_Dad_Bandana",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5114
	},

	[40315] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Red Neko Mimi",
		identifiedResourceName = "Red_Neko_Mimi",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5115
	},

	[40316] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Red Pirate Hat",
		identifiedResourceName = "Red_Pirate_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5116
	},

	[40317] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Red Scarf",
		identifiedResourceName = "Red_Scarf",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5117
	},

	[40318] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Red Valkyrie Helm",
		identifiedResourceName = "Red_Valkyrie_Helm",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5118
	},

	[40319] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Scarf",
		identifiedResourceName = "Scarf",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5119
	},

	[40320] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Silver Feather Beret",
		identifiedResourceName = "Silver_Feather_Beret",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5120
	},

	[40321] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Silver Galapago Cap",
		identifiedResourceName = "Silver_Galapago_Cap",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5121
	},

	[40322] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "Silver Neko Mimi",
		identifiedResourceName = "Silver_Neko_Mimi",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5122
	},

	[40323] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "White Cowboy Hat",
		identifiedResourceName = "White_Cowboy_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5123
	},

	[40324] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "White Feather Beret",
		identifiedResourceName = "White_Feather_Beret",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5124
	},

	[40325] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "White Galapago Cap",
		identifiedResourceName = "White_Galapago_Cap",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5125
	},

	[40326] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "White Gangster Scarf",
		identifiedResourceName = "White_Gangster_Scarf",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5126
	},

	[40327] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "White Love Dad Bandana",
		identifiedResourceName = "White_Love_Dad_Bandana",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5127
	},

	[40328] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "White Luxury Hat",
		identifiedResourceName = "White_Luxury_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5128
	},

	[40329] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "White Neko Mimi",
		identifiedResourceName = "White_Neko_Mimi",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5129
	},

	[40330] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "White Pirate Hat",
		identifiedResourceName = "White_Pirate_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5130
	},

	[40331] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "White Ship Captain Hat",
		identifiedResourceName = "White_Ship_Captain_Hat",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5131
	},

	[40332] = {
		unidentifiedDisplayName = "Hat",
		unidentifiedResourceName = "��",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by using a ^6666CCMagnifier^000000." },
		identifiedDisplayName = "White Valkyrie Helm",
		identifiedResourceName = "White_Valkyrie_Helm",
		identifiedDescriptionName = {
			"A beautiful and awesome costume made by kids from china.",
			"Class:^6666CC Headgear^000000",
			"Defense:^0000FF 0^000000",
			"Position:^6666CC Top^000000",
			"Weight:^009900 60^000000",
			"Jobs:^6666CC All except Novice^000000"
		},
		slotCount = 0,
		ClassNum = 5132
	},

	
}

for ItemID, DESC in pairs(tbl_items_custom) do
	CheckItem(ItemID, DESC)
end